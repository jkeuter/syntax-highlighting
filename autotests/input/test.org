* Heading

Regular *bold* /italic/ _underline_ +strike-through+ ~code~ text.

** TODO Heading [1/3]
  
+ [X] First item
+ [-] Second item
  - [X] Sub-item
  - [ ] Sub-item 2

     
1. Number list
2. [ ] Number list with checkbox

#+BEGIN_BLOCK
Inside a block
#+END_BLOCK
